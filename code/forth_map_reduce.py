import common as c
import string
import sys

alphanum = string.ascii_letters + string.digits
validchars = alphanum + ",./;'\"+-\n!@#$%^&*() "

import collections
import itertools
import multiprocessing


##
# The following is from:
# http://pymotw.com/2/multiprocessing/mapreduce.html
##
class SimpleMapReduce(object):
    
    def __init__(self, map_func, reduce_func, num_workers=None):
        """
        map_func

          Function to map inputs to intermediate data. Takes as
          argument one input value and returns a tuple with the key
          and a value to be reduced.
        
        reduce_func

          Function to reduce partitioned version of intermediate data
          to final output. Takes as argument a key as produced by
          map_func and a sequence of the values associated with that
          key.
         
        num_workers

          The number of workers to create in the pool. Defaults to the
          number of CPUs available on the current host.
        """
        self.map_func = map_func
        self.reduce_func = reduce_func
        self.pool = multiprocessing.Pool(num_workers)
    
    def partition(self, values):
        """Organize the mapped values by their key.
        Returns an unsorted sequence of tuples with a key and a sequence of values.
        """
        # partitioned_data = collections.defaultdict(list)
        # for key, value in mapped_values:
        #     partitioned_data[key].append(value)
        # for s in values:
        #     print(s)
        return values #partitioned_data.items()
    
    def __call__(self, inputs, chunksize=1):
        """Process the inputs through the map and reduce functions given.
        
        inputs
          An iterable containing the input data to be processed.
        
        chunksize=1
          The portion of the input data to hand to each worker.  This
          can be used to tune performance during the mapping phase.
        """
        map_responses = self.pool.map(self.map_func, inputs, chunksize=chunksize)
        # print("map_responses: %s" % map_responses)
        partitioned_data = self.partition(itertools.chain(map_responses))
        reduced_values = filter(lambda x: x is not None, self.pool.map(self.reduce_func, partitioned_data))
        return reduced_values


def scanLine(line):
    curr_value = 0
    line = line.strip()
    high_score = 0.0
    values = {0.0: ""}

    # print("%s: line: %s" % (multiprocessing.current_process().name, line))
    for x in alphanum:
        secret_code = []
        for y in c.dHex(line.strip()):
            char = c.xor_strings(x, y)
            if char in validchars:
                secret_code.append(char)
            else:
                break
        if len(secret_code) < len(line) / 2:
            continue
        curr_value = c.score_function(''.join(secret_code))
        if curr_value > high_score:
            values[curr_value] = "%s:%s" % (curr_value, ''.join(secret_code))
            high_score = curr_value
    return (high_score, values[high_score])


def reducer(results):
    if results[0] > 0:
        return results

if __name__ == '__main__':
    lines = []
    if len(sys.argv) == 2:
        source_file = sys.argv[1]
    else:
        source_file = 'gistfile1.txt'

    with open(source_file, 'r') as f:
        lines = f.readlines()

    print("Read in %s lines" % len(lines))
    mapper = SimpleMapReduce(scanLine, reducer, 1)
    output = mapper(lines)
    print(output)


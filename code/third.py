import common as c
import string

code = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

msg_str = c.dHex(code)
output = []

alphanum = string.ascii_lowercase + string.ascii_uppercase + ''.join([str(x) for x in range(0,10)])
print(alphanum)
def run_through_chars(cipher):
    prev_value = 0.0
    results = {}
    output = []
    for x in alphanum.upper():
        for y in c.dHex(cipher):
            output.append(c.xor_strings(x,y))
        curr_value = score_function(''.join(output))
        if curr_value > prev_value:
            print("Key %s scored: %s showing string: %s" % (x, curr_value, ''.join(output)))
            results[curr_value] = "%s:%s" % (x, ''.join(output))
            prev_value = curr_value
        output = []
    print("Mystery string seems to be:")
    print(results[prev_value])


def score_function(mystery_string):
    s = 0
    VOWELS = 'aeiou '
    for x in mystery_string.lower():
        if x in VOWELS:
            s += 1
    # print("S is: %s \t score is: %s" % (s, float(s) / len(mystery_string)))

    return float(s)/len(mystery_string)


if __name__ == '__main__':
    run_through_chars(code)

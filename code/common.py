"""
Various helper functions in one place
"""


def dHex(a):
    return a.decode("hex")


def enHex(a):
    return a.encode("hex")


def enB64(a):
    return a.encode("base64")


def xor_strings(a, b):
    return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b))


def score_function(mystery_string):
    if len(mystery_string) == 0:
        return 0.0
    s = 0
    VOWELS = 'aeiou '
    for x in mystery_string.lower():
        if x in VOWELS:
            s += 1
    # print("S is: %s \t score is: %s" % (s, float(s) / len(mystery_string)))
    return float(s) / len(mystery_string)


def hamming_distance(first, second):
    if len(first) != len(second):
        return -1
    return sum(a != b for a, b in zip(first, second))

import common as c
import string

alphanum = string.ascii_letters + string.digits #+ "!@#$%^&*()-_+=<>,./;:\" ?{}|\\"
validchars = alphanum + ",./;'\"+-!@#\n$%^&*() "
lines = []
with open('gistfile1.txt', 'r') as f:
    lines = f.readlines()

print("Read in %s lines" % len(lines))

highscore = -1.0
secret_code = []
results = {}
line_count = 1
for line in lines:
    # print("line: %d" % line_count)
    for x in alphanum:
        for y in c.dHex(line.strip()):
            char = c.xor_strings(x, y)
            if char in validchars:
                secret_code.append(char)
            else:
                break
        tmp = ''.join(secret_code)
        curr_value = c.score_function(tmp.strip())
        if len(secret_code) < len(line) / 2:
            secret_code = []
            continue
        if curr_value > highscore:
            results[curr_value] = "%s:%s" % (x, ''.join(secret_code))
            highscore = curr_value
        secret_code = []
    line_count += 1
print("Mystery string seems to be:")
print(results[highscore])
print(highscore)
print("All results:")
print(results)



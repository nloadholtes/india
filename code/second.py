first = "1c0111001f010100061a024b53535009181c"
key = "686974207468652062756c6c277320657965"

expected = "746865206b696420646f6e277420706c6179"


def second():
    hex_str = first.decode("hex")
    return xor_strings(hex_str, key.decode("hex")).encode("hex")

def xor_strings(a, b):
    return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(a, b))

print(second())
assert second() == expected


import common as c

assert c.enHex(c.xor_strings(c.dHex(first), c.dHex(key))) == expected
import common as c

INPUT = """Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal"""
EXPECTED = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"

KEY = "ICE"

output = []
for x in range(0, len(INPUT), 3):
    output.append(c.xor_strings(INPUT[x:x + 3], KEY))
output = c.enHex("".join(output))

print(output)
print(EXPECTED)

assert output, EXPECTED
